import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DinosuarGameComponent } from './dinosuar-game/dinosuar-game.component';

@NgModule({
  declarations: [
    AppComponent,
    DinosuarGameComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

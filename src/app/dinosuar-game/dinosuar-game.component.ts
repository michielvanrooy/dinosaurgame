import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Enemy } from '../model/enemy';
import { Player } from '../model/player';
import { EnemyType } from '../enum/enemy-type';
import { GameLevel } from '../enum/game-level';

@Component({
  selector: 'dinosuar-game',
  templateUrl: './dinosuar-game.component.html',
  styleUrls: ['./dinosuar-game.component.css']
})
export class DinosuarGameComponent implements OnInit {

  public enemies: Enemy[] = [];
  public enemyCount = 100;
  public horizontalLine : number = 520; 
  public player: Player = new Player(this.horizontalLine);
  public score : number = 0;
  public level : number = 1; //TODO: Delete
  public enemySpawnRate : number = 100;


  @ViewChild("myCanvas") canvas: ElementRef;

  @HostListener('document:keypress', ['$event'])
  @HostListener('document:keydown', ['$event'])
  @HostListener('document:keyup', ['$event'])

  handleKeyboardEvent(event: KeyboardEvent) {

    if(event.type == "keypress") {
      if(event.key == 'w') {
        this.player.jump();
      }
    }

    if(event.type == "keydown") {
      if(event.key == 's') {
        this.player.duck();
      }
    }

    if(event.type == "keyup") {
      if(event.key == 's') {
        this.player.standUp();
      }
    }
  }

  public constructor() { }

  public ngOnInit() {
    this.enemies.push(new Enemy(this.getRandomEnemy(), this.getLevel(), this.horizontalLine));  
    this.animate();
  }

  public animate() {
    const myCanvas: HTMLCanvasElement = this.canvas.nativeElement;

    if(this.enemyCount == 0)
    {
      this.enemyCount = this.enemySpawnRate;
      this.generateEnemy();
    }
    this.enemyCount -= 1;

    let resultId = requestAnimationFrame(this.animate.bind(this));
    var ctx = myCanvas.getContext('2d');
    ctx.clearRect(0, 0, 1000, 1000);
    ctx.moveTo(0, this.horizontalLine);
    ctx.lineTo(1000, this.horizontalLine);
    ctx.stroke();

    this.player.animate(myCanvas);

    this.enemies.forEach(enemy => {
      enemy.animate(myCanvas);
    });

    if (this.checkGameOver()) {
      cancelAnimationFrame(resultId);
      resultId = null;
    }

    this.removeEnemies();

    this.score++;
  }

  public generateEnemy()
  {
    this.enemies.push(new Enemy(this.getRandomEnemy(), this.getLevel(), this.horizontalLine));
    
  }

  public removeEnemies()
  {
    for(let i = 0; i < this.enemies.length; i++)
    {
      if(this.enemies[i].x < 0)
      {
        this.enemies.splice(i, 1);
      }
    }
  }
  
  public checkGameOver(): boolean {

    for(let i = 0; i < this.enemies.length; i++)
    {
      if ((this.enemies[i].x >= this.player.x && this.enemies[i].x <= this.player.x + this.player.width) ||
      (this.enemies[i].x + this.enemies[i].width >= this.player.x && this.enemies[i].x + this.enemies[i].width  <= this.player.x + this.player.width)) {

        let eyTop = this.enemies[i].y;
        let eyBottom = this.enemies[i].y + this.enemies[i].height;

        let pyTop = this.player.y + (this.player.initHeight - this.player.height);
        let pyBottom =   this.player.y + this.player.height;

        if ((eyTop >= pyTop && eyTop <= pyBottom) || (eyBottom >= pyTop && eyBottom <= pyBottom)) {   
          return true;
        }
      }
    }
    return false;
  }

  public getRandomEnemy() : EnemyType {
    const enumValues = Object.keys(EnemyType)
    .map(n => Number.parseInt(n))
    .filter(n => !Number.isNaN(n))
    Math.random()
    let randomIndex = Math.floor(Math.random() * (enumValues.length + 1));

    return EnemyType[EnemyType[randomIndex]];
  }

  public getLevel() : GameLevel {

    if (this.score <= 500) {
      this.enemySpawnRate = 100;
      return GameLevel.Level1;
    }
    
    if (this.score <= 1000) {
      this.level = 2;
      this.enemySpawnRate = 90;
      return GameLevel.Level2;
    }

    if (this.score <= 2000) {
      this.level = 3;
      this.enemySpawnRate = 80;
      return GameLevel.Level3;
      
    }

    this.level = 4;
    this.enemySpawnRate = 70;
    return GameLevel.Level4;
  }


}



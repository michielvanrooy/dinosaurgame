import { EnemyType } from "../enum/enemy-type";
import { GameLevel } from "../enum/game-level";


export class Enemy
{
    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public horizontalLine : number;
    public movementSpeed : number;

    constructor(enemyType: EnemyType, gameLevel : GameLevel, horizontalLine: number)
    {
        this.horizontalLine = horizontalLine;
        this.x = 1000;
        this.enemyBuilder(enemyType, gameLevel);
    }

    public move(vilocity: number)
    {
        this.x -= vilocity;
    }

    public animate(canvas : HTMLCanvasElement)
    {
        
        this.move(this.movementSpeed);

        var ctx = canvas.getContext('2d');
        ctx.fillStyle = "#000000";
        ctx.fillRect(this.x, this.y, this.width, this.height );
    }

    public enemyBuilder(type: EnemyType, level : GameLevel)
    {
        switch(type)
        {
            case EnemyType.SmallBush:
                this.height = 20;
                this.width = 20;
                this.y = this.horizontalLine - this.height;
                break;
            case EnemyType.LargeBush:
                this.height = 20;
                this.width = 40;
                this.y = this.horizontalLine - this.height;
                break;
            case EnemyType.Tree:
                this.height = 40;
                this.width = 20;
                this.y = this.horizontalLine - this.height;
                break;
            case EnemyType.LowBird:
                this.height = 20;
                this.width = 20;
                this.y = this.horizontalLine - this.height - 30;
                break;
            case EnemyType.HighBird:
                this.height = 20;
                this.width = 20;
                this.y = this.horizontalLine - this.height - 50;
                break;
            default:
                //smallbush
                this.height = 20;
                this.width = 20;
                this.y = this.horizontalLine - this.height;
                break;
        }

        switch(level)
        {
            case GameLevel.Level1:
                this.movementSpeed = 5;
                break;
            case GameLevel.Level2:
                this.movementSpeed = 7;
                break;
            case GameLevel.Level3:
                this.movementSpeed = 10;
                break;
            case GameLevel.Level4:
                this.movementSpeed = 12;
                break;
            default:
                this.movementSpeed = 5;
                break;
        }
    }
}
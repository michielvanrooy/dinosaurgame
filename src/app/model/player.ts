export class Player {

    public initHeight: number;
    public height: number;
    public x: number;
    public y: number;
    public width: number;
    public horizontalLine : number;
    private isJumping: boolean;
    private isDucking: boolean;
    private velocity: number;
    private jumpHeight: number;
    private jumpDirection: Direction = Direction.Up;
    

    constructor(horizontalLine: number) {
        this.horizontalLine = horizontalLine;
        this.height = 40;
        this.initHeight = this.height;
        this.x = 30;
        this.y = this.horizontalLine - this.height;
        this.velocity = 6;
        this.jumpHeight = 90;
        this.width = 20;
    }

    public animate(canvas: HTMLCanvasElement) {

        this.animateJump();
        this.animateDuck();

        let newY = (this.isDucking) ? this.y + this.height : this.y;

        var ctx = canvas.getContext('2d');
        ctx.fillStyle = "#FF0000";
        ctx.fillRect(this.x, newY, 20, this.height);
    }

    public jump() {
        this.isJumping = true;
    }

    public duck() {
        this.isDucking = true;
    }

    public standUp() {
        this.isDucking = false;
    }

    public animateJump() {
        if (this.isJumping) {
            if (this.jumpDirection == Direction.Up) {
                this.y -= this.velocity;
                if (this.y <= this.horizontalLine - this.jumpHeight - this.height) {   //lineHeigh - jumpHight - playerHeight
                    this.jumpDirection = Direction.Down;
                }
            }
            else {
                this.y += this.velocity;
                if (this.y >= (this.horizontalLine - this.initHeight)) {
                    this.jumpDirection = Direction.Up;
                    this.isJumping = false;
                }
            }
        }
    }

    public animateDuck() {
        if (this.isDucking) {
            this.height = 20;
        }
        else {
            this.height = this.initHeight;
        }
    }
}

export enum Direction {
    Up,
    Down,
    Left,
    Right
  }

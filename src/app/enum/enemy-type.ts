export enum EnemyType {
    SmallBush,
    LargeBush,
    Tree,
    HighBird,
    LowBird
  }